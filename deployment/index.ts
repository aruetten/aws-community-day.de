import { App, Stack } from '@aws-cdk/core';
import { SinglePageAppHosting } from 'taimos-cdk-constructs';

class FrontendStack extends Stack {
    constructor(parent: App) {
        super(parent, 'website-aws-community-day-de', { env: { region: 'eu-central-1' } });
        this.templateOptions.description = 'Frontend for www.aws-community-day.de';

        new SinglePageAppHosting(this, 'Frontend', {
            certArn: 'arn:aws:acm:us-east-1:292004443359:certificate/fa47be67-83a5-46be-bc6b-c275576ae200',
            zoneId: 'Z125WMCY1JN2OE',
            zoneName: 'aws-community-day.de',
        });
    }
}

const app = new App();
new FrontendStack(app);
app.synth();
